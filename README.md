# UCSC PPS Assignment_07

**Arrays in C**

1. [Write a C program to reverse a sentence entered by user.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_07/-/tree/main/Q1%20-%20Reverse%20a%20sentence)
    - [Using new Array.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_07/-/blob/main/Q1%20-%20Reverse%20a%20sentence/PPS_A7_Q1_revSen_newArray.c)
    - [Using strrev() function.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_07/-/blob/main/Q1%20-%20Reverse%20a%20sentence/PPS_A7_Q1_revSen_strrev.c)
    - [Using a swapping method.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_07/-/blob/main/Q1%20-%20Reverse%20a%20sentence/PPS_A7_Q1_revSen_swapping.c)<br><br>
2. [Write a C program to find the Frequency of a given character in a given string.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_07/-/blob/main/Q2%20-%20Frequency%20of%20a%20character/PPS_A7_Q2_chFrq.c)<br><br>
3. [Write a C program to add and multiply two given matrixes.](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_07/-/tree/main/Q3%20-%20Add%20and%20multiply%202%20matrixes)
    - [Addition](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_07/-/blob/main/Q3%20-%20Add%20and%20multiply%202%20matrixes/PPS_A7_Q3_addMatrix.c)
    - [Multiplication](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_07/-/blob/main/Q3%20-%20Add%20and%20multiply%202%20matrixes/PPS_A7_Q3_mulMatrix.c)
    - [Full Code](https://gitlab.com/is1101-programming-and-problem-solving/ucsc-pps-assignment_07/-/blob/main/Q3%20-%20Add%20and%20multiply%202%20matrixes/PPS_A7_Q3_fullCode.c)
