//SALU Dissanayake - 202029
//IS1101 Programming and Problem Solving
//Assignment 07 - Question 03
//Write a C program to add two given matrixes.

#include <stdio.h>

void inputMatrix(int arr[10][10]);
void addMatrix();

int r, c;
int arr1[10][10], arr2[10][10];
int add[10][10];

int main()
{
    printf("\nEnter no of rows: ");
    scanf("%d", &r);
    printf("Enter no of columns: ");
    scanf("%d", &c);

    printf("\n---Enter the elements of 1st matrix---\n");
    inputMatrix(arr1);

    printf("\n---Enter the elements of 2nd matrix---\n");
    inputMatrix(arr2);

    printf("\n---Addition of entered matrices---\n");
    addMatrix();

    printf("\n");
    return 0;
}

void inputMatrix(int arr[10][10])
{
    for (int i = 0; i < r; i++)
    {
        for (int j = 0; j < c; j++)
        {
            printf("Enter the element of %d,%d: ", i + 1, j + 1);
            scanf("%d", &arr[i][j]);
        }
    }
}

void addMatrix()
{
    for (int i = 0; i < r; i++)
    {
        for (int j = 0; j < c; j++)
        {
            add[i][j] = arr1[i][j] + arr2[i][j];
            printf("%d  ", add[i][j]);
            if (j == c - 1)
            {
                printf("\n");
            }
        }
    }
}
