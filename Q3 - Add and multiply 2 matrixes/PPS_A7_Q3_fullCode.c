#include <stdio.h>

int r, c;
int r1, r2, c1, c2;

int arr1[10][10], arr2[10][10];
int add[10][10];
int mul[10][10];

void inputAddMatrix(int arr[10][10]);
void addMatrix();

void createMatrix();
void inputMulMatrix(int arr[10][10], int r, int c);
void mulMatrix();
void printMatrix();

int main()
{
    char operator;

    printf("Press 1 for Addition\nPress 2 for Multiplication\n");
    printf("Enter an operator: ");
    scanf("%c", &operator);

    switch (operator)
    {
    case '1':
        printf("\nEnter no of rows: ");
        scanf("%d", &r);
        printf("Enter no of columns: ");
        scanf("%d", &c);

        printf("\n---Enter the elements of 1st matrix---\n");
        inputAddMatrix(arr1);

        printf("\n---Enter the elements of 2nd matrix---\n");
        inputAddMatrix(arr2);

        printf("\n---Addition of entered matrices---\n");
        addMatrix();

        printf("\n");

        break;
    case '2':
        createMatrix();
        while (c1 != r2)
        {
            printf("\nError! column of 1st matrix not equal to row of 2nd matrix.\n");
            createMatrix();
        }

        printf("\n---Enter the elements of 1st matrix---\n");
        inputMulMatrix(arr1, r1, c1);

        printf("\n---Enter the elements of 2nd matrix---\n");
        inputMulMatrix(arr2, r2, c2);

        mulMatrix();

        printf("\n---Multiplication of entered matrices---\n");
        printMatrix();

        printf("\n");
        break;
    default:
        printf("Input error!!!");
    }
}

void inputAddMatrix(int arr[10][10])
{
    for (int i = 0; i < r; i++)
    {
        for (int j = 0; j < c; j++)
        {
            printf("Enter the element of %d,%d: ", i + 1, j + 1);
            scanf("%d", &arr[i][j]);
        }
    }
}

void addMatrix()
{
    for (int i = 0; i < r; i++)
    {
        for (int j = 0; j < c; j++)
        {
            add[i][j] = arr1[i][j] + arr2[i][j];
            printf("%d  ", add[i][j]);
            if (j == c - 1)
            {
                printf("\n");
            }
        }
    }
}

void createMatrix()
{
    printf("\nEnter no of rows for 1st matrix: ");
    scanf("%d", &r1);
    printf("Enter no of columns for 1st matrix: ");
    scanf("%d", &c1);
    printf("\nEnter no of rows for 2nd matrix: ");
    scanf("%d", &r2);
    printf("Enter no of columns for 2nd matrix: ");
    scanf("%d", &c2);
}

void inputMulMatrix(int arr[10][10], int r, int c)
{
    for (int i = 0; i < r; i++)
    {
        for (int j = 0; j < c; j++)
        {
            printf("Enter the element of %d,%d: ", i + 1, j + 1);
            scanf("%d", &arr[i][j]);
        }
    }
}

void mulMatrix()
{
    for (int i = 0; i < r1; i++)
    {
        for (int j = 0; j < c2; j++)
        {
            for (int k = 0; k < c1; k++)
            {
                mul[i][j] += arr1[i][k] * arr2[k][j];
            }
        }
    }
}

void printMatrix()
{
    for (int i = 0; i < r1; i++)
    {
        for (int j = 0; j < c2; j++)
        {
            printf("%d  ", mul[i][j]);
            if (j == c2 - 1)
            {
                printf("\n");
            }
        }
    }
}
