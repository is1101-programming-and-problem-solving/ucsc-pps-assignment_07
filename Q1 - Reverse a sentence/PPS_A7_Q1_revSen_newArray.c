//SALU Dissanayake - 202029
//IS1101 Programming and Problem Solving
//Assignment 07 - Question 01
//Write a C program to reverse a sentence entered by user.
//Using new Array.

#include <stdio.h>
#include <string.h>

int main()
{
    char str[50], rev[50];
    int length, end, i;

    printf("Enter a sentence: ");
    gets(str);

    length = strlen(str);
    end = length - 1;

    /*
    length = 0;
    while (str[length] != '\0')
    {
        length++;
    }
    end = length - 1;
    */

    for (i = 0; i < length; i++)
    {
        rev[i] = str[end];
        end--;
    }

    rev[i] = '\0';

    printf("%s", rev);

    return 0;
}
