//SALU Dissanayake - 202029
//IS1101 Programming and Problem Solving
//Assignment 07 - Question 01
//Write a C program to reverse a sentence entered by user.
//Using a swapping method.

#include <stdio.h>
#include <string.h>

void reverseStr(char str[50]);

int main()
{
    char str[50];

    printf("Enter a sentence: ");
    gets(str);

    reverseStr(str);
    printf("%s", str);
    
    return 0;
}

void reverseStr(char str[50])
{
    int length = strlen(str);

    for (int i = 0; i < length / 2; i++)
    {
        char ch = str[i];
        str[i] = str[length - i - 1];
        str[length - i - 1] = ch;
    }
}
