//SALU Dissanayake - 202029
//IS1101 Programming and Problem Solving
//Assignment 07 - Question 01
//Write a C program to reverse a sentence entered by user.
//Using strrev() function.

#include <stdio.h>
#include <string.h>

int main()
{
    char str[50];

    printf("Enter a sentence: ");
    gets(str);

    strrev(str);

    printf("%s", str);

    return 0;
}
