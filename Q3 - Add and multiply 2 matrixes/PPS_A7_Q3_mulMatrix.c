//SALU Dissanayake - 202029
//IS1101 Programming and Problem Solving
//Assignment 07 - Question 03
//Write a C program to multiply two given matrixes.

#include <stdio.h>

void createMatrix();
void inputMatrix(int arr[10][10], int r, int c);
void mulMatrix();
void printMatrix();

int r1, r2, c1, c2;
int arr1[10][10], arr2[10][10];
int mul[10][10];

int main()
{
    createMatrix();
    while (c1 != r2)
    {
        printf("\nError! column of 1st matrix not equal to row of 2nd matrix.\n");
        createMatrix();
    }

    printf("\n---Enter the elements of 1st matrix---\n");
    inputMatrix(arr1, r1, c1);

    printf("\n---Enter the elements of 2nd matrix---\n");
    inputMatrix(arr2, r2, c2);

    mulMatrix();

    printf("\n---Multiplication of entered matrices---\n");
    printMatrix();

    printf("\n");
    return 0;
}

void createMatrix()
{
    printf("\nEnter no of rows for 1st matrix: ");
    scanf("%d", &r1);
    printf("Enter no of columns for 1st matrix: ");
    scanf("%d", &c1);
    printf("\nEnter no of rows for 2nd matrix: ");
    scanf("%d", &r2);
    printf("Enter no of columns for 2nd matrix: ");
    scanf("%d", &c2);
}

void inputMatrix(int arr[10][10], int r, int c)
{
    for (int i = 0; i < r; i++)
    {
        for (int j = 0; j < c; j++)
        {
            printf("Enter the element of %d,%d: ", i + 1, j + 1);
            scanf("%d", &arr[i][j]);
        }
    }
}

void mulMatrix()
{
    for (int i = 0; i < r1; i++)
    {
        for (int j = 0; j < c2; j++)
        {
            for (int k = 0; k < c1; k++)
            {
                mul[i][j] += arr1[i][k] * arr2[k][j];
            }
        }
    }
}

void printMatrix()
{
    for (int i = 0; i < r1; i++)
    {
        for (int j = 0; j < c2; j++)
        {
            printf("%d  ", mul[i][j]);
            if (j == c2 - 1)
            {
                printf("\n");
            }
        }
    }
}
