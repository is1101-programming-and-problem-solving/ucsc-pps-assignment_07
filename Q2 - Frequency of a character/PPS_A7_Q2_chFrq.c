//SALU Dissanayake - 202029
//IS1101 Programming and Problem Solving
//Assignment 07 - Question 02
//Write a C program to find the Frequency of a given character in a given string.

#include <stdio.h>

int main()
{
    char str[100], ch;
    int frq = 0;

    printf("Enter a string: ");
    gets(str);

    printf("Enter a character: ");
    scanf("%c", &ch);

    for (int i = 0; str[i] != '\0'; i++)
    {
        if (ch == str[i])
            frq++;
    }

    printf("Frequency of %c = %d", ch, frq);

    return 0;
}
